<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{
    public function create(){
        return view('cast.create');
    }

    public function store(Request $request){
        $request->validate([
            'nama' => 'required', 
            'umur' => 'required',
            'bio' => 'required',
            ]);

        DB::table('cast')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio'  => $request['bio']
         ]);

         return redirect('/cast');
        }

         public function index(){
             $castview = DB::table('cast')->get();
             return view('cast.index', compact('castview'));
         }

         public function show($id){
            $castshow = DB::table('cast')->where('id', $id)->first();
            return view('cast.show', compact('castshow'));
         }

         public function edit($id){
            $castedit = DB::table('cast')->where('id', $id)->first();
            return view('cast.edit', compact('castedit'));
         }

         public function update($id, Request $request){
            $request->validate([
                'nama' => 'required', 
                'umur' => 'required',
                'bio' => 'required',
                ]);

            $castedit = DB::table('cast')
                ->where('id', $id)
                ->update([
                    'nama' => $request['nama'],
                    'umur' => $request['umur'],
                    'bio' => $request['bio']
                ]);

            return redirect('/cast');
         }

            public function destroy($id){
                DB::table('cast')->where('id', $id)->delete();
            return redirect('/cast');
            }
}

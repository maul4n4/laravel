@extends('layout.template')

@section('judul')
Edit Cast {{$castedit->nama}}   
@endsection

@section('content')
<form action="/cast/{{$castedit->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label>Nama</label>
      <input type="text" name="nama" value="{{$castedit->nama}}" class="form-control">
      </div>
      @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <div class="form-group">
        <label>Umur</label>
        <input type="number" name="umur" value="{{$castedit->umur}}" class="form-control">
        </div>
        @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <div class="form-group">
      <label>Bio</label>
      <textarea name="bio" class="form-control" cols="30" rows="10">{{$castedit->bio}}"</textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
  @endsection
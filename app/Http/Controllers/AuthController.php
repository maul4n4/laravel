<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
  public function signup(){
      return view('halaman.form');
  }  

  public function selamat(Request $request){
        $nama_depan = $request->fist_name;
        $nama_belakang = $request->last_name;
        return view('halaman.welcome', compact('nama_depan', 'nama_belakang'));
    }
}

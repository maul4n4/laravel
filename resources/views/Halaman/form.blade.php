@extends('layout.template')

@section('judul')
Buat Account Baru!
Sign Up Form	
@endsection

@section('content')
<form action="/welcome" method="post">
	@csrf
	  <label for="fist_name">Fist Name: </label> <br>
		<input type="text"  id="fist_name">
		<br><br>
		<label for="last_name">Last Name: </label> <br>
		<input type="text" id="last_name">
		<br><br>
		<label>Gender: </label><br>
		<input type="radio" name="gender"> Male <br>
		<input type="radio" name="gender"> Female <br>
		<input type="radio" name="gender"> Other <br>
		<br>
		<label>Nationality:</label><br>
		<select>
		<option>Indonesian</option>
			<option>Malaysia</option>
			<option>Australia</option>
		</select>
		<br><br>
		<label>Language Spoken: </label><br>
		<input type="checkbox" name="language_spoken" value="0">Indonesian<br>
		<input type="checkbox" name="language_spoken" value="1">English <br>
		<input type="checkbox" name="language_spoken" value="2">Other<br>
		<br>
		<label>Bio:</label><br>
		<textarea cols="50" rows="7"></textarea><br>
		<input type="submit" value="Sign Up">
		</form>
@endsection

